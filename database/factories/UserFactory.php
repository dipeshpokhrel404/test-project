<?php

namespace Database\Factories;

use App\Models\Role;
use App\Models\User;
use Illuminate\Database\Eloquent\Factories\Factory;
use Illuminate\Support\Facades\Hash;
use Illuminate\Support\Str;

class UserFactory extends Factory
{
    /**
     * The name of the factory's corresponding model.
     *
     * @var string
     */
    protected $model = User::class;

    /**
     * Define the model's default state.
     *
     * @return array
     */
    public function definition(): array
    {
        return [
            'first_name'      => $this->faker->firstName(),
            'last_name'       => $this->faker->lastName(),
            'email'           => $this->faker->unique()->safeEmail(),
            'password'        => Hash::make('password'), // password
            'last_logged_in'  => now(),
            'authorized_date' => now()
        ];
    }

    /**
     * Set user role.
     *
     * @return Factory
     */
    public function role(): Factory
    {
        return $this->state(function (array $attributes) {
            return [
                'role_id' => Role::all()->random(1)->first()->id,
            ];
        });
    }
}
