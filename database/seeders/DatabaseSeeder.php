<?php

namespace Database\Seeders;

use App\Models\User;
use Illuminate\Database\Seeder;

class DatabaseSeeder extends Seeder
{
    /**
     * Seed the application's database.
     *
     * @return void
     */
    public function run()
    {
        //Seeder to create default role
        $this->call(RoleTableSeeder::class);

        //Factory to create default 100 users with role at a time
        User::factory(100)->role()->create();
    }
}
