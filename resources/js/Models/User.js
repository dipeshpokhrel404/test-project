import axios from "axios";

export default {
    //Get all users from database
    all() {
        return axios.get('/user/all');
    },
    //Update given user with given role
    updateRole(user_id, role_id) {
        return axios.patch(`/user/${user_id}/role`, {role_id: role_id});
    }
};
