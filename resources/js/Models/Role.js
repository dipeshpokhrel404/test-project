import axios from "axios";

export default {
    //Get all roles from database
    all() {
        return axios.get('/role/all');
    }
}
