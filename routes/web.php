<?php

use App\Http\Controllers\RoleController;
use App\Http\Controllers\UserController;
use Illuminate\Support\Facades\Route;

/*
|--------------------------------------------------------------------------
| Web Routes
|--------------------------------------------------------------------------
|
| Here is where you can register web routes for your application. These
| routes are loaded by the RouteServiceProvider within a group which
| contains the "web" middleware group. Now create something great!
|
*/

Route::get('/', function () {
    return view('main');
});

//Get all users with role
Route::get('user/all', [UserController::class, 'index']);

//Update user role
Route::patch('user/{id}/role', [UserController::class, 'updateRole']);

//Get all roles
Route::get('role/all', [RoleController::class, 'index']);
