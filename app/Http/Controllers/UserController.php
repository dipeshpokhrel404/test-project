<?php

namespace App\Http\Controllers;

use App\Http\Requests\UserRoleRequest;
use App\Models\User;
use Illuminate\Database\Eloquent\Builder;
use Illuminate\Database\Eloquent\Collection;

class UserController extends Controller
{
    /**
     * Get all roles
     *
     * @return Builder[]|Collection
     */
    public function index()
    {
        return User::with('role')->get();
    }

    /**
     * Update User Role
     *
     * @param UserRoleRequest $request
     * @param int $id
     * @return mixed
     */
    public function updateRole(UserRoleRequest $request, int $id)
    {
        return User::findOrFail($id)->update($request->only('role_id'));
    }
}
