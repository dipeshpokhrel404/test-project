<?php

namespace App\Http\Controllers;

use App\Models\Role;
use Illuminate\Database\Eloquent\Collection;

class RoleController extends Controller
{
    /**
     * Get all roles
     *
     * @return Role[]|Collection
     */
    public function index()
    {
        return Role::all(['title as text', 'id as value']);
    }
}
